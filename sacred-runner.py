#!/usr/bin/python3
import os
import subprocess
import time
import sacred
from sacred.observers import MongoObserver

def time_statistics(filename):
    command_list = ['/home/feng/triangles/build/nvtc/nvtc-variant', '-f', filename]
    start = time.time()
    subprocess.run(command_list)
    elapsed = time.time() - start
    return elapsed

ex = sacred.Experiment('tr_counting')
user_name = os.environ.get('SACRED_USER', 'admin')
user_passwd = os.environ.get('SACRED_PASSWD', 'abc')
collection_url = 'mongodb://%s:%s@10.8.4.170/?authSource=user-data'%(user_name, user_passwd)

if(os.sys.platform != 'win32' and os.environ.get('USE_MONGO')):
    ex.observers.append(MongoObserver.create(
        url=collection_url,
        db_name='sacred'))

@ex.config
def cfg():
    filename = '/home/dataset/triangle_counting_dataset/s28.e15.kron.edgelist.bin'

@ex.automain
def run(filename):
    return time_statistics(filename)